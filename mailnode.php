<?php
/**
 * Plugin Name: MailNode
 * Author Name: Raj
 * Author URI: http://www.nodesman.com
 * Description: An email marketing plugin for WordPress
 * Tags: email marketing, autoresponder, newsletter, blog post deliveries
 */

$plugin_dir = dirname(__FILE__);

include_once($plugin_dir.'/system/NApp.php');
include_once($plugin_dir.'/NConfig.php');

class NBroadcaster extends NApp {

    private static $instance;

    public function __construct($config) {
        self::$configuration = $config;
        parent::__construct();
    }

    public static function getInstance($NEmail_Config) {

        if (!self::$instance) {
            self::$instance = new NBroadcaster($NEmail_Config);
        }

        return self::$instance;
    }

    public function init() {
        do_action('javelin_init');
    }

    public function admin_init() {
        parent::admin_init();

        do_action('javelin_init');
    }

}

add_action('init', array(NBroadcaster::getInstance($NEmail_Config), 'init'));
add_action('admin_init', array(NBroadcaster::getInstance($NEmail_Config), 'admin_init'));