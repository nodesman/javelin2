<?php


$NEmail_Config = array(

    "admin_menu" => array(

        "title" => 'Newsletters',
        "menu_title" => 'Newsletters',
        "capability" => 'activate_plugins',
        "menu_slug" => 'nodemail/newsletters',
        'callback' => array('NewslettersController', 'home'),
        'items' => array(
            'title'=>'Newsletters',
            'menu_title'=>'Newsletters',
            'capability'=>'activate_plugins',
            'nodemail/newsletters',
            'callback' =>array('NewsletterController', 'home')
        )
    )

)

?>